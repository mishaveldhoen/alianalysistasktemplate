AliAnalysisTaskTemplate
=======================

The Template task is a minimal task, supposed to run on any data type. It only uses methods from the virtual event and track classes. The task can be used as a basis for more compex tasks, which should be committed to separate branches, the master should always remain simple.
