#ifndef ALIANALYSISTASKTEMPLATE_H
#define ALIANALYSISTASKTEMPLATE_H

class AliAnalysisTaskTemplate : public AliAnalysisTaskSE {

public:
	AliAnalysisTaskTemplate();
	AliAnalysisTaskTemplate(const char* name);
	virtual ~AliAnalysisTaskTemplate();

	virtual void UserCreateOutputObjects();
	virtual void UserExec(Option_t*);
	virtual void Terminate(Option_t*);

	void SetIsAOD(Bool_t isAOD = kTRUE) {fIsAOD = isAOD;}	

private:
	TList* 			fOutputList;		//!
	TH1F*			fPtDistribution;	//!
	Bool_t			fIsAOD;				//!

	ClassDef(AliAnalysisTaskTemplate,1);

};

#endif