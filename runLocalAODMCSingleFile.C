// Run the analysis on a single file.
//const char* gFilePath = "/Users/mishaveldhoen/Downloads/AliAOD.root";
const char* gFilePath = "/Users/mishaveldhoen/LocalData/sim/2013/LHC13e3a/139510/AOD/010/AliAOD.root";

void runLocalAODMCSingleFile() {

    // Standard Libraries.
	gSystem->Load("libCore.so");
	gSystem->Load("libTree.so");
	gSystem->Load("libGeom.so");
	gSystem->Load("libVMC.so");
	gSystem->Load("libPhysics.so");
	gSystem->Load("libSTEERBase");
	gSystem->Load("libAOD");
	gSystem->Load("libANALYSIS");
	gSystem->Load("libANALYSISalice");
    
	gROOT->ProcessLine(".include $ALICE_ROOT/include");
	
	AliAnalysisManager *mgr = new AliAnalysisManager("AnalysisManager");
	AliAODInputHandler *aodH = new AliAODInputHandler();
	mgr->SetInputEventHandler(aodH);

	// Compile libraries and Analysis Task.
	gROOT->LoadMacro("AliAnalysisTaskTemplate.cxx++g");	

	// Add Analysis Task.
    gROOT->LoadMacro("AddTaskTemplate.C");
    AliAnalysisTaskTemplate* UserTask = AddTaskTemplate("SingleFile.root");

    // Some final adjustments to the user task.
    UserTask->SetDebugLevel(10);
    UserTask->SelectCollisionCandidates(AliVEvent::kMB);

	mgr->SetDebugLevel(10);
	if (!mgr->InitAnalysis()) return;
	mgr->PrintStatus();
	
	TChain *analysisChain = new TChain("aodTree");

	// Connect to grid if the file is there, or if we want to use a histogram from the grid.	
	if (TString(gFilePath).Contains("alien://")) {TGrid::Connect("alien://");}

	analysisChain->Add(gFilePath);

	mgr->StartAnalysis("local", analysisChain);

}
