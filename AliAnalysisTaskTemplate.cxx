#include <iostream>

#include "TH1F.h"
#include "TChain.h"

#include "AliVEvent.h"
#include "AliVTrack.h"

#include "AliInputEventHandler.h"
#include "AliAnalysisManager.h"

#include "AliAnalysisTaskSE.h"
#include "AliAnalysisTaskTemplate.h"

using namespace std;

ClassImp(AliAnalysisTaskTemplate);

// -------------------------------------------------------------------------
AliAnalysisTaskTemplate::AliAnalysisTaskTemplate():
	AliAnalysisTaskSE(),
	fOutputList(0x0),
	fPtDistribution(0x0),
	fIsAOD(kTRUE)

{

	// Default constructor.
	if (fDebug > 1) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

}

// -------------------------------------------------------------------------
AliAnalysisTaskTemplate::AliAnalysisTaskTemplate(const char* name):
	AliAnalysisTaskSE(name),
	fOutputList(0x0),
	fPtDistribution(0x0),
	fIsAOD(kTRUE)

{

	// Named constructor.
	if (fDebug > 1) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	DefineInput(0,TChain::Class());
	DefineOutput(1, TList::Class());

}

// -------------------------------------------------------------------------
AliAnalysisTaskTemplate::~AliAnalysisTaskTemplate() {}

// -------------------------------------------------------------------------
void AliAnalysisTaskTemplate::UserCreateOutputObjects() {

	if (fDebug > 1) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	// Create the output list.
	fOutputList = new TList();
	fOutputList->SetOwner(kTRUE);

	// Create Spectrum.
	fPtDistribution = new TH1F("fPtDistribution","P_{T} Distribution;p_{T};N_{ch}",100,0.,10.);
	fOutputList->Add(fPtDistribution);

	PostData(1,fOutputList);

}

// -------------------------------------------------------------------------
void AliAnalysisTaskTemplate::UserExec(Option_t*) {

	if (fDebug > 1) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}

	AliVEvent* currentEvent = InputEvent();

	// Input the event handler.
	AliInputEventHandler* InputHandler = (AliInputEventHandler*)((AliAnalysisManager::GetAnalysisManager())->GetInputEventHandler());
	if (!InputHandler) return;

	// Select min. bias events.
	UInt_t trigger = InputHandler->IsEventSelected();
	if (!(trigger & AliVEvent::kMB)) {return;}

	// Fill Pt distribution.
	for (Int_t iTrack = 0; iTrack < currentEvent->GetNumberOfTracks(); iTrack++) {

		AliVTrack* currentTrack = (AliVTrack*)currentEvent->GetTrack(iTrack);
		fPtDistribution->Fill(currentTrack->Pt());

	}

	PostData(1,fOutputList);

}
// -------------------------------------------------------------------------
void AliAnalysisTaskTemplate::Terminate(Option_t*) {

	if (fDebug > 1) {cout << Form("File: %s, Line: %i, Function: %s",__FILE__,__LINE__,__func__) << endl;}
	
}
